<?php

namespace Modules\Tours\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;

class TourOrder extends Model
{
    public function tourtime()
    {
    	return $this->belongsTo(TourTime::class, 'tour_time_id');
    }
    
    public function tour()
    {
    	return $this->belongsTo(Tour::class, 'tour_id');
    }
    
    public function when2(){
        return date('Y-m-d H:i', strtotime($this->when));
    }
    
    public function setOrderStatus($status="Laukiama"){
        if($this->type=="regular") $status = "Patvirtinta";
        if($this->status!=$status){
            $this->status = $status;
            switch ($status) {
                case "Laukiama":

                break;
                case "Patvirtinta":
                    if ($this->type == 'regular') {
                        sendReservationMail($this);
                    } else {
                        Mail::send(new \Karuselle\Mail\PatvirtintaEmail($this));
                    }
                    break;
                case "Atsaukta":
                    Mail::send(new \Karuselle\Mail\AtsauktaEmail($this));
                break;
            }
            $this->save();
            return true;
        }
        return false;
    }
    
    public function getTotalSum(){
        return $this->member*$this->getPrice();
    }
    
    public function getPrice(){
        if($this->type == 'regular') return $this->tourtime->tour->regular_price;

        if($this->type == 'private') {
            if($this->tour->private_tour_prices) {
                $group_sizes = json_decode($this->tour->private_tour_prices, true);
                if($group_sizes) {
                    foreach ($group_sizes as $key => $ptp) {
                        $from = $ptp['from'];
                        $to = $ptp['to'];
                        $price = $ptp['price'];

                        if($from <= $this->member){
                            if(($to > 0 && $to >= $this->member) || $to == -1) {
                                return $price;
                            }
                        }
                    }
                }
            }
        }

        return 0;
    }

}
