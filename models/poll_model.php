<?php
class Poll_model extends CI_Model {

	private $table = 'poll'; 
	private $table_results = 'poll_questions'; 

	public function get_polls_data() 
	{
		$query = $this->db->get($this->table);
		return $query;	
	}

	public function get_poll_data($id) 
	{
		$query = $this->db->get_where($this->table, array('id' => $id));
		foreach ($query->result() as $row)
		{
		    return $row;
		}
	}

	public function get_all_poll_questions($id) 
	{
		$query = $this->db->get_where($this->table_results, array('poll_id' => $id));
		return $query;
	}

	public function create_poll()
	{
		if($this->db->insert($this->table, $this->input->post())) {
			return true;
		} else {
			die($this->db->_error_message());
		}
	}

	public function update_poll($id)
	{
		$this->db->where('id', $id);
		$this->db->update($this->table, $this->input->post()); 
		return true;
	}

	public function delete_poll($id) 
	{
		if($this->db->delete($this->table, array('id' => $id))) {
			$this->db->where('poll_id', $id);
			$this->db->delete($this->table_results);
			return true; 
		} else {
			die($this->db->_error_message());
		}
	}

	public function get_current_question($id) 
	{
		$query = $this->db->get_where($this->table_results, array('question_id' => $id));
		foreach ($query->result() as $row)
		{
		    return $row;
		}
	}

	public function create_question()
	{
		if($this->db->insert($this->table_results, $this->input->post())) {
			return true;
		} else {
			die($this->db->_error_message());
		}
	}

	public function update_question($id)
	{
		$this->db->where('question_id', $id);
		$this->db->update($this->table_results, $this->input->post()); 
		return true;
	}

	public function delete_question($id) 
	{
		if($this->db->delete($this->table_results, array('question_id' => $id))) {
			return true; 
		} else {
			die($this->db->_error_message());
		}
	}

	public function insert_result()
	{
		$qid = $this->input->post('question_id');
		$current_q = $this->get_current_question($qid);
		$data=array('results' => ++$current_q->results);
		$this->db->where('question_id', $qid);
		$this->db->update($this->table_results, $data); 
		return true;
	}
}
?>
