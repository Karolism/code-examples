<?php

namespace Modules\Reservations\Entities;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $table = 'reservation_services';
    protected $guarded = [];

    public function translation()
    {
        return $this->hasOne(ServiceTranslation::class, 'service_id');
    }

    public function serviceSuppliers()
    {
        return $this->belongsToMany(Supplier::class, 'reservation_suppliers_services', 'service_id', 'supplier_id');
    }

    /**
     * Get services with their translations
     * @param string $language
     * @param int $paginate
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Database\Eloquent\Collection|static[]
     */
    public static function getServicesWithTranslations($language, $paginate=0)
    {
        $servicesObj = self::with(['translation'=>function ($query) use ($language){
            return $query->where('lang', $language);
        }]);

        if (is_numeric($paginate) && $paginate>0) {
            return $servicesObj->paginate($paginate);
        } else {
            return $servicesObj->get();
        }
    }

    /**
     * Get single service with it's translation
     * @param int $id
     * @param string $language
     * @return Model|null|object|static
     */
    public static function getServiceWithTranslation($id, $language)
    {
        return self::with(['translation'=>function($query) use ($language) {
            return $query->where('lang', $language);
        }])->where('id', $id)->first();
    }

}
