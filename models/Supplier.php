<?php

namespace Modules\Reservations\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Supplier extends Model
{
    protected $table = 'reservation_suppliers';
    protected $guarded = [];

    public function translation()
    {
        return $this->hasOne(SupplierTranslation::class, 'supplier_id');
    }

    public function services()
    {
        return $this->belongsToMany(Supplier::class, 'reservation_suppliers_services', 'supplier_id', 'service_id');
    }

    /**
     * Get suppliers with their translations
     * @param string $language
     * @param int $paginate
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Database\Eloquent\Collection|static[]
     */
    public static function getSuppliersWithTranslations($language, $paginate=0)
    {
        $suppliersObj = self::with(['translation'=>function ($query) use ($language){
            return $query->where('lang', $language);
        }]);

        if (is_numeric($paginate) && $paginate>0) {
            return $suppliersObj->paginate($paginate);
        } else {
            return $suppliersObj->get();
        }
    }

    /**
     * Get single supplier with it's translation
     * @param int $id
     * @param string $language
     * @return Model|null|object|static
     */
    public static function getSupplierWithTranslation($id, $language)
    {
        return self::with(['translation'=>function($query) use ($language) {
            return $query->where('lang', $language);
        }])->where('id', $id)->first();
    }

    /**
     * Perform search for suppliers
     * @param int $serviceId
     * @param string $language
     * @param string $searchText
     * @return mixed
     */
    public static function searchForSuppliers($serviceId, $language, $searchText)
    {
        $suppliersObj = self::select(DB::raw("reservation_suppliers.id, reservation_suppliers_translations.title AS text"))
            ->join('reservation_suppliers_translations', 'reservation_suppliers.id', '=', 'reservation_suppliers_translations.supplier_id')
            ->join('reservation_suppliers_services', 'reservation_suppliers.id', '=', 'reservation_suppliers_services.supplier_id')
            ->where('lang', $language)
            ->where('service_id', $serviceId);
        if (!empty($searchText)) {
            $suppliersObj->where('title', 'like', '%'.$searchText.'%');
        }
        return $suppliersObj->get();
    }

}
