<?php

namespace Modules\Gallery\Entities;

use Illuminate\Foundation\Auth\User;
use Modules\Media\Entities\Media;
use Modules\Media\Entities\Upload;
use Illuminate\Database\Eloquent\Model;
use Modules\Users\Entities\UserDetail;
use Modules\Languages\Entities\Language;

class Video extends Model {

    protected $table = 'videos';
    protected $fillable = [
        'create_author_id',
        'update_author_id',
        'media_id',
        'active',
        'date',
    ];

    public function translation() {
        return $this->hasOne(VideoTranslation::class);
    }

    public function translations() {
        return $this->hasMany(VideoTranslation::class);
    }

    public function media() {
        return $this->hasOne(Media::class, 'id', 'media_id');
    }

    public function create_author() {
        return $this->hasOne(User::class, "id", "create_author_id");
    }

    public function update_author() {
        return $this->hasOne(User::class, "id", "update_author_id");
    }

    public function getUrl() {
        $slug = $this->slug($this->translation->slug, $this->id);

        return url('/n' . $this->id . '-' . $slug);
    }


    private function slug($slug, $id = '') {
        $language = Language::getAdminLanguage();

        $p = VideoTranslation::where('slug', $slug)->where('language', $language)->first();
        if (count($p)) {
            $i = 1;
            while (true) {
                $slug = $slug . '-' . $i;

                $p = VideoTranslation::where('slug', $slug)->where('gallery_id', '!=', $id)->where('language', $language)->first();
                if (!count($p))
                    break;

                $i++;
            }
        }

        return $slug;
    }

}
