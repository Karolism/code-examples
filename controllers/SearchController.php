<?php

namespace IDcms\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Route;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Carbon\Carbon;
use Modules\Categories\Entities\Category;
use Modules\Languages\Entities\Language;
use Modules\Pages\Entities\Page;
use Modules\Pages\Entities\PageTranslation;
use Modules\Blog\Entities\Article;
use Modules\Blog\Entities\Category as BlogCategory;

class SearchController extends Controller {
       
    public function index() {
        $language = LaravelLocalization::getCurrentLocale();

        $search_string = '';
        if (isset($_GET['search'])) {
            $search_string = $_GET['search'];

         $news = Article::whereHas('translation', function($q)  use ($language, $search_string) {
              $q->where('language', $language);
                $q->where(function($query) use ($search_string){
                    $query->where('title','like','%'.$search_string.'%')
                        ->orWhere('description','like','%'.$search_string.'%');
                });
            })->with(['translation' => function($q) use($language, $search_string) {
                
                $q->where('language', $language);
                $q->where(function($query) use ($search_string){
                    $query->where('title','like','%'.$search_string.'%')
                        ->orWhere('description','like','%'.$search_string.'%');
                });
            }])
            ->with('media')
            ->with('create_author')
            ->with('update_author')
            ->get();
            if ($news->count() == 0) {
                $news = [];
            }
            
            
        }
        else {
            $news = [];
        }
        
        $image = \Modules\Media\Entities\Media::where('id', get_setting('settings_page_header_image'))->get()->first();
        $args = [
            'language' => $language,
            'news' => $news,
            'image' => $image
        ];

        return view('search/index', $args);
    }
    
}
