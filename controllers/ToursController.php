<?php

namespace Modules\Tours\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Validator;
use URL;
use Module;
use Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;

use Modules\Tours\Entities\Tour;
use Modules\Tours\Entities\TourTranslation;
use Modules\Tours\Entities\SeoData;
use Modules\Tours\Entities\Document;
use Modules\Languages\Entities\Language;
use Modules\Categories\Entities\Category;
use Modules\Tours\Entities\TourDocument;

use RecursiveArrayIterator;
use RecursiveIteratorIterator;

class ToursController extends Controller {
    private $languages, $default_language, $_default_language;

    public function __construct() {
        $this->default_language = Language::where('language_default', 1)->first();
        $this->_default_language = Language::getDefault();
        $this->languages = Language::orderBy('language_default', 'desc')->get();
    }

    public function get_index() {
        $language = Language::getAdminLanguage();

        $nodes = Tour::whereHas('translation', function($query)  use ($language) {
            $query->where('language', $language);
        })->with(['translation' => function($query) use($language) {
            $query->where('language', $language);
        }])->orderBy('ord', 'asc')->paginate(10);

        $tours_content = '';
        foreach ($nodes as $node) {
            $title = '';

            if ( ! isset($node->translation->title))
                continue;

            $title .= '<strong>' . $node->translation->title .' '. $node->translation->headline . '</strong>';
            
            $_tour = $node;
            $id =  $node->id;
            $toursList = '';
            $_toursList = [];

            $tours_content .= view('tours::parts.tour_line', [
                'tour' => $node,
                'title' => $title
            ]);
            $tours_content .= view('tours::parts.tour_edit_line', [
                'tour' => $node,
                'tours_list' => $toursList
            ]);
        }

        return view('tours::dashboard', [
            'tours_content' => $tours_content,
            'nodes' => $nodes
        ]);
    }

    public function getTreeContent($array) {
        $traverse = function ($categories, $content = '', $margin = '') use (&$traverse) {
            foreach ($categories as $category) {
                $content .= view('tours::parts.tour_line', [
                    'tour' => $category,
                    'margin' => $margin
                ]);

                if (is_array($category->children)) {
                    $content .= view('tours::parts.tour_line', [
                        'data' => $category,
                        'margin' => $margin
                    ]);
                    $content .= $traverse($category->children, $content, $margin . '—');
                }

                if ( ! is_array($category->children)) {
                    $content = $traverse($category->children, $content, $margin . '—');
                }
            }

            return $content;
        };

        return $traverse($array);
    }

    public function get_create(Request $request, $language = '') {
        if ( ! $language)
            $language = Language::getDefault();

        if ( ! $request->session()->get('added_tour_id')) {
            $tour = new Tour();
            $tour->save();

            $request->session()->put('added_tour_id', $tour->id);
        } else {
            $tour_id = $request->session()->get('added_tour_id');
            $tour = Tour::find($tour_id);
        }

        $tour_translation = TourTranslation::where('tour_id', $tour->id)
            ->where('language', $language)->first();
        if(!$tour_translation) {
            $tour_translation = new TourTranslation();
            $tour_translation->tour_id = $tour->id;
            $tour_translation->language = $language;
            $tour_translation->save();
        }

        return view('tours::tour', [
            'is_new' => true,
            'tour' => $tour,
            'language' => $language,
            'languages' => $this->languages,
            'documents' => [],
        ]);
    }

    public function post_ajaxFormatSlug(Request $request) {
        $str = $request->str;
        $id = $request->id;

        $slug = $this->slug($str, $id);
        $link = $this->getLink($id);
        $path = url($link . '/tour/' . $slug);
        $path = str_replace('//', '/', $path);

        return response()->json([
            'slug' => $slug,
            'full_url' => $path
        ]);
    }

    private function slug($str, $id = '') {
        $slug = str_slug($str);
        $language = Language::getAdminLanguage();

        $p = TourTranslation::where('slug', $slug)->where('tour_id', '!=', $id)->where('language', $language)->first();
        if (count($p)) {
            $i = 1;
            while (true) {
                $slug = $slug . '-' . $i;

                $p = TourTranslation::where('slug', $slug)->where('tour_id', '!=', $id)->where('language', $language)->first();
                if ( ! count($p))
                    break;

                $i++;
            }
        }

        return $slug;
    }

    public function get_edit($id, $language = '', Request $request) {
        $language = Language::getAdminLanguage();

        $_tour = Tour::with([
            'translation' => function($query) use ($language) {
                $query->where('language', '=', $language)
                        ->with(['informations' => function($query){$query->orderBy('ord', 'asc');}]);
            },
            'seo' => function($query) use ($language) {
                $query->where('post_type', 'tour')->where('language', $language);
            },
            'documents', 'times'
        ])->find($id);


        $private_tour_prices = json_decode($_tour->private_tour_prices, true);
        //return $_tour;
        return view('tours::tour', [
            'is_new' => false,
            'tour' => $_tour,
            'language' => $language,
            'private_tour_prices' => $private_tour_prices
        ]);
    }

    public function post_save(Request $request) {
        $new_tour = false;
        
        if($request->post('info_submit')){
            $validator = Validator::make($request->all(), [
                'title' => 'required',
                'info_banner' => 'required',
                'info_description' => 'required'
            ]);
        }
        else $validator = Validator::make($request->all(), [
            'title' => 'required'
        ]);

        if ($validator->fails()) {
            return back() ->withErrors($validator)->withInput();
        }

        $lang = Language::getAdminLanguage();

        $status = 0;
        if ($request->status == 'on')
            $status = 1;
        
        $published = 0;
        if (isset($request->published) && $request->published == 1) {
            $published = 1;
        }

        if ($request->id) {
            $tour = Tour::find($request->id);
        } else {
            $tour = new Tour();
            $new_tour = true;
        }
        $tour->img_id = $request->img_id;
        $tour->published = $published;
        $tour->language = $lang;
        $tour->latitude = $request->latitude;
        $tour->longitude = $request->longitude;
        $tour->tourlangs = $request->tourlangs;
        $tour->memcount = $request->memcount;
        $tour->regular_price = $request->regular_price;
        $tour->private_price = $request->private_price;
        
        if (!is_numeric($request->ord)) {
            $tour->ord = Tour::count() + 1;
        }
        else {
            $tour->ord = $request->ord;
        }

        //private tours prices
        $tour->private_tour_prices = json_encode($request->ptp);
        $tour->save();  

        $slug = $this->slug($request->slug, $request->id);

        // save translation
        $tourTranslation = TourTranslation::where('tour_id', $request->id)->where('language', $lang)->first();
        if ($tourTranslation) {
            $tourTranslation->title = $request->title;
            $tourTranslation->headline = $request->headline;
            $tourTranslation->slug = $slug;
            $tourTranslation->content = $request->tour_description;
            $tourTranslation->highlights = $request->highlights;
            $tourTranslation->language = $lang;
            $tourTranslation->duration = $request->duration;
            $tourTranslation->placename = $request->placename;
            $tourTranslation->save();
        } else {
            $tourTranslation = new TourTranslation;
            $tourTranslation->title = $request->title;
            $tourTranslation->headline = $request->headline;
            $tourTranslation->slug = $slug;
            $tourTranslation->content = $request->tour_description;
            $tourTranslation->highlights = $request->highlights;
            $tourTranslation->language = $lang;
            $tourTranslation->tour_id = $tour->id;
            $tourTranslation->duration = $request->duration;
            $tourTranslation->placename = $request->placename;
            $tourTranslation->save();
        }

        // save seo data
        $seo = SeoData::where('post_id', $request->id)->where('language', $lang)->where('post_type', 'tour')->first();
        if ($seo) {
            $seo->title = $request->meta_title;
            $seo->keywords = $request->meta_keywords;
            $seo->description = $request->meta_description;
            $seo->language = $lang;
            $seo->save();
        } else {
            $seoData = new SeoData;
            $seoData->title = $request->meta_title;
            $seoData->keywords = $request->meta_keywords;
            $seoData->description = $request->meta_description;
            $seoData->language = $lang;
            $seoData->post_id = $tour->id;
            $seoData->post_type = 'tour';
            $seoData->save();
        }

        // save documents
        TourDocument::where('tour_id', $tour->id)->delete();

        if ($request->media_id) {
            foreach ($request->media_id as $key => $id) {
                if ( ! $id) continue;

                $doc = new TourDocument();
                $doc->tour_id = $tour->id;
                $doc->media_id = $id;
                $doc->save();
            }
        }
        
        if($request->post('info_submit')){
            $doc = new \Modules\Tours\Entities\TourInformation();
            $doc->tour_translation_id = $tourTranslation->id;
            $doc->content = $request->info_description;
            $doc->img_id = $request->info_banner;
            $doc->ord = \Modules\Tours\Entities\TourInformation::where('tour_translation_id', $tourTranslation->id)->count() + 1;
            $doc->save();
            
            
        }
        
        $info_data = json_decode($request->info_data);
        
        if($info_data) foreach ($info_data as $key=>$value){
            if($key+1!=$value->sort){
                $toui = \Modules\Tours\Entities\TourInformation::find($value->id);
                $toui->ord = $key+1;
                $toui->save();
            }
        }

        $request->session()->forget('added_tour_id');

        if ($new_tour) {
            return redirect()
                ->route('admin.tours.get_edit', ['id' => $tour->id, 'lang' => $lang])
                ->with('message:success', 'Turas sukurtas sėkmingai');
        } else {
            return redirect()
                ->route('admin.tours.get_edit', ['id' => $tour->id, 'lang' => $lang])
                ->with('message:success', 'Turas atnaujintas sėkmingai');
        }
    }

    public function post_saveFastEdit(Request $request) {
        $validator = Validator::make($request->all(), [
            'title' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'success',
                'message' => 'Įveskite turo pavadinimą'
            ]);
        }

        $language = Language::getAdminLanguage();


        $tour = Tour::find($request->id);
        
        if (!is_numeric($request->ord)) {
            $tour->ord = Tour::count() + 1;
        }
        else {
            $tour->ord = $request->ord;
        }
        
        $tour->save();

        // save translation
        $tourTranslation = TourTranslation::where('tour_id', $request->id)->where('language', $language)->first();
        $tourTranslation->title = $request->title;
        $tourTranslation->save();

        // get html
        $id = $request->id;
        $toursList = '';

        $_tour = Tour::with([
            'translation' => function($query) use ($language) {
                $query->where('language', '=', $language);
            },
            'seo' => function($query) use ($language) {
                $query->where('post_type', 'tour')->where('language', $language);
            },
            'documents'
        ])->find($id);

        $title = '<strong>' . $_tour->translation->title . '</strong>';

        $tour_line = view('tours::parts.tour_line', [
            'tour' => $_tour,
            'title' => $title
        ])->render();

        $tour_edit_line = view('tours::parts.tour_edit_line', [
            'tour' => $_tour,
            'tours_list' => $toursList
        ])->render();

        return response()->json([
            'type' => "success",
            'message' => 'Touras atnaujintas sėkmingai',
            'tour_line' => $tour_line,
            'tour_line_edit' => $tour_edit_line
        ]);
    }

    public function post_changeStatus(Request $request) {
        $id = $request->id;

        $tour = Tour::find($id);

        if ($tour->published == 1) {
            $tour->published = 0;
        } else {
            $tour->published = 1;
        }

        $tour->save();

        return response()->json([
            'type' => 'success',
            'published' => $tour->published,
            'message' => 'Turo būsena sėkmingai pakeista'
        ]);
    }

    public function post_delete(Request $request) {
        $id = $request->id;

        $docs = TourDocument::where('tour_id', $id)->get();
        if (count($docs)) {
            foreach ($docs as $doc) {
                $doc->delete();
            }
        }
        
        $docs = TourTranslation::where('tour_id', $id)->get();
        if (count($docs)) {
            foreach ($docs as $doc) {
                $docs2 = \Modules\Tours\Entities\TourInformation::where('tour_translation_id', $doc->id)->get();
                if (count($docs2)) {
                    foreach ($docs2 as $doc2) {
                        $doc2->delete();
                    }
                }
                $doc->delete();
            }
        }

        Tour::find($id)->delete();
        
        SeoData::where('post_id', $id)->where('post_type', 'tour')->delete();

        return response()->json([
            'status' => 'success'
        ]);
    }

    function getLink($id) {
        $default_language = Language::getDefault();
        $language = Language::getAdminLanguage();

        $link = '';
        if ($default_language != $language) {
            $link = $language . '/';
        }

        return $link;
    }

    function post_removeDocument(Request $request) {
        $id = $request->id;

        $doc = TourDocument::find($id);
        if (count($doc)) {
            $doc->delete();
        }

        return response()->json([
            'status' => 'success'
        ]);
    }
    
    public function post_save_info_item(Request $request)
    {
            $mi = \Modules\Tours\Entities\TourInformation::find($request->item_id);
            $mi->content = $request->content;
            $saved = $mi->save();

            if ( $saved ) {
                    return \response()->json(['status'=>'success','message'=>'Išsaugota']);
            }

            return \response()->json(['status'=>'error','message'=>'Išsaugoti nepavyko']);
    }
    
    public function post_removeInfoItem(Request $request) {
        $menu_id = $request->info_id;
        $mi = \Modules\Tours\Entities\TourInformation::find($menu_id);
        $mi->delete();

        return response()->json([
            'status' => 'success'
        ]);
    }
    
    public function post_save_date_item(Request $request)
    {
        if(strtotime($request->date)<time()) return \response()->json(['status'=>'error','message'=>'Laikas turi būti vėliau nei dabar']);
        $mi = \Modules\Tours\Entities\TourTime::where('when', date("Y-m-d H:i:s", strtotime($request->date)))->where('tour_id', $request->tour_id);
        if($mi->count()) return \response()->json(['status'=>'error','message'=>'Toks laikas jau yra']);
        $mi = new \Modules\Tours\Entities\TourTime;
        $mi->when = date("Y-m-d H:i:s", strtotime($request->date));
        $mi->tour_id = $request->tour_id;
        $saved = $mi->save();

        if ( $saved ) {
                return \response()->json(['status'=>'success','message'=>'Išsaugota', 'when' => $mi->when2(), 'id' => $mi->id]);
        }

        return \response()->json(['status'=>'error','message'=>'Išsaugoti nepavyko']);
    }
    
    public function post_removeDateItem(Request $request) {
        $menu_id = $request->id;
        $mi = \Modules\Tours\Entities\TourTime::find($menu_id);
        $mi->delete();

        return response()->json([
            'status' => 'success'
        ]);
    }

    public function get_groupSizeLine(Request $request) {
        if($request->ajax()) {
            $start = (int)$request->start ? (int)$request->start : 1;
            $end = $start + 10;
            $is_new = true;
            $html = '';
            $html .= view('tours::parts.group_size', compact('start', 'end', 'is_new'));
            return response()->json(compact('html'));
        }
    }
}
