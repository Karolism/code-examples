<?php

namespace Modules\Reservations\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Modules\Languages\Entities\Language;
use Modules\Reservations\Entities\Order;
use Modules\Reservations\Entities\OrderItem;
use Modules\Reservations\Entities\Service;
use Modules\Reservations\Entities\Supplier;
use Modules\Reservations\Entities\ReservationTime;
use Modules\Reservations\Entities\Customer;

class OrdersController extends Controller
{
    /**
     * Display a list of orders.
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $language = Language::getAdminLanguage();

        return view('reservations::orders.index', [
            'orders'=>Order::getFilteredOrdersList($request->all()),
            'language'=>$language,
            'orderStates'=>get_array_of_classificators_from_settings('reservation_orders_states_'.$language),
            'services'=>Service::getServicesWithTranslations($language),
            'suppliers'=>Supplier::getSuppliersWithTranslations($language)
        ]);
    }

    /**
     * Show the form for creating a new order.
     * @return Response
     */
    public function create()
    {
        $language = Language::getAdminLanguage();
        $order = new Order();
        return view('reservations::orders.order_form', [
            'order'=>$order,
            'new'=>true,
            'language'=>$language,
            'services'=>Service::getServicesWithTranslations($language)
        ]);
    }

    /**
     * Validate order
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function validate(Request $request)
    {
        $status = 'success';
        $message = '';
        if (empty($request->name)) {
            $status = 'error';
            $message = _e('Įveskite užsakovo vardą', 'admin.reservations');
        }
        if (empty($request->surname)) {
            $status = 'error';
            $message = _e('Įveskite užsakovo pavardę', 'admin.reservations');
        }
        if (empty($request->order_state_id)) {
            $status = 'error';
            $message = _e('Jūs privalote pasirinkti užsakymo statusą', 'admin.reservations');
        }
        if (!isset($request->service_id) || count($request->service_id)==0) {
            $status = 'error';
            $message = _e('Užsakymas turi turėti bent vieną rezervacijos laiką', 'admin.reservations');
        }

        $orderLines = [];
        if (isset($request->service_id)) {
            foreach ($request->service_id as $key=>$orderLine) {
                if (!is_numeric($request->service_id[$key])) {
                    $status = 'error';
                    $message = _e('Jūs privalote pasirinkti paslaugą', 'admin.reservations');
                }
                if (!is_numeric($request->supplier_id[$key])) {
                    $status = 'error';
                    $message = _e('Jūs privalote pasirinkti paslaugos teikėją', 'admin.reservations');
                }
                if (!is_numeric($request->time_id[$key])) {
                    $status = 'error';
                    $message = _e('Jūs privalote pasirinkti rezervacijos laiką', 'admin.reservations');
                }
                if (empty($request->reservation_date[$key])) {
                    $status = 'error';
                    $message = _e('Jūs privalote pasirinkti rezervacijos datą', 'admin.reservations');
                }

                $orderLine = [
                    $request->supplier_id[$key],
                    $request->time_id[$key],
                    $request->reservation_date[$key]
                ];
                if (in_array($orderLine, $orderLines)) {
                    $status = 'error';
                    $message = _e('Užsakyme yra eilu�?ių, kurių teikėjas, data ir laikas yra vienodi', 'admin.reservations');
                }
                $orderLines[] = $orderLine;
            }
        }
        return response()->json([
            'status' => $status,
            'message' => $message
        ]);
    }

    /**
     * Save newly created order info or update current order info
     * @param  Request $request
     * @return Response
     */
    public function save(Request $request)
    {
        $language = Language::getAdminLanguage();

        $customer = Customer::where('name', $request->name)
            ->where('surname', $request->surname)
            ->first();
        if (!is_object($customer)) {
            $customer = new Customer();
        }
        $customer->name = $request->name;
        $customer->surname = $request->surname;
        $customer->telephone = $request->telephone;
        $customer->email = $request->email;
        $customer->save();

        $order = Order::find($request->order_id);
        if (!$order) {
            $order = new Order();
        }
        $order->order_state_id = $request->order_state_id;
        $order->customer_id = $customer->id;
        $order->payed = isset($request->payed) ? 1 : 0;
        $order->notes = $request->notes;
        $order->updated_at = date('Y-m-d H:i:s');
        $order->save();

        $idsOfOrderItems =[];
        foreach($request->order_item_id as $orderItemId) {
            if (is_numeric($orderItemId)) {
                $idsOfOrderItems[] = $orderItemId;
            }
        }

        if (count($idsOfOrderItems)>0) {
            OrderItem::where('order_id', $order->id)->whereNotIn('id', $idsOfOrderItems)->delete();
        } else {
            OrderItem::where('order_id', $order->id)->delete();
        }

        $sumOfOrder = 0;
        $price = 0;
        foreach($request->service_id as $key=>$orderLine) {

            if (is_numeric($request->order_item_id[$key])) {
                $orderItem = OrderItem::find($request->order_item_id[$key]);
            } else {
                $orderItem = new OrderItem();
            }

            $orderItem->service_id = $request->service_id[$key];
            $service = Service::getServiceWithTranslation($request->service_id[$key], $language);
            if (is_object($service) && is_object($service->translation)) {
                $orderItem->service_title = $service->translation->title;
            }

            $orderItem->supplier_id = $request->supplier_id[$key];
            $supplier = Supplier::getSupplierWithTranslation($request->supplier_id[$key], $language);
            if (is_object($supplier) && is_object($supplier->translation)) {
                $orderItem->supplier_title = $supplier->translation->title;
            }

            $orderItem->order_id = $order->id;
            $orderItem->time_id = $request->time_id[$key];

            $reservationDate = $request->reservation_date[$key];

            $reservationTime = ReservationTime::find($request->time_id[$key]);
            if (is_object($reservationTime)) {
                $reservationTime = ReservationTime::setAttributes($reservationTime);

                $orderItem->datetime_from = $reservationDate.' '.$reservationTime->reservation_time_from;
                $orderItem->datetime_to = $reservationDate.' '.$reservationTime->reservation_time_to;

                $sumOfOrder = 0;
                if (strtotime($reservationTime->discount_date_from)<=time() && strtotime($reservationTime->discount_date_to)>=time() && $reservationTime->price_with_discount>0) {
                    $price = $reservationTime->price_with_discount;
                } elseif ($reservationTime->price>0) {
                    $price = $reservationTime->price;
                } elseif (strtotime($supplier->discount_date_from)<=time() && strtotime($supplier->discount_date_to)>=time() && $supplier->price_with_discount>0) {
                    $price = $supplier->price_with_discount;
                } elseif ($supplier->price>0) {
                    $price = $supplier->price;
                } elseif (strtotime($service->discount_date_from)<=time() && strtotime($service->discount_date_to)>=time() && $service->price_with_discount>0) {
                    $price = $service->price_with_discount;
                } else {
                    $price = $service->price;
                }
                $orderItem->price = $price;
            }

            $orderItem->save();

            $sumOfOrder = $sumOfOrder + $price;
        }


        if (!$request->new) {
            return back()->with('message:success', _e('Užsakymas sėkmingai atnaujintas', 'admin.reservations'));
        } else {
            return redirect()->route('admin.reservations.orders.edit', ['id'=>$order->id])
                ->with('message:success', _e('Užsakymas sėkmingai sukurtas', 'admin.reservations'));
        }
    }

    /**
     * Show the form for editing the specified order.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function edit(Request $request, $id)
    {
        $language = Language::getAdminLanguage();

        $order = Order::with([
            'customer',
            'orderItems',
            'orderItems.reservationTime',
            'orderItems.reservationTime.supplier',
            'orderItems.service'
        ])->find($id);

        return view('reservations::orders.order_form', [
            'order'=>$order,
            'new'=>false,
            'language'=>$language,
            'services'=>Service::getServicesWithTranslations($language),
            'suppliers'=>Supplier::getSuppliersWithTranslations($language)
        ]);
    }

    /**
     * Remove the specified order and order items from storage.
     * @param Request $request
     * @return Response
     */
    public function delete(Request $request)
    {
        OrderItem::where('order_id', $request->id)->delete();
        Order::find($request->id)->delete();
        return response()->json([
            'status'=>'success'
        ]);
    }

    /**
     * Get options of suppliers
     * @param Request $request
     * @return mixed
     */
    public function getOptionsOfSuppliers(Request $request)
    {
        $language = Language::getAdminLanguage();
        return [
            'suppliers'=>Supplier::searchForSuppliers($request->service_id, $language, $request->search_text)
        ];
    }

    /**
     * Get options of reservation times
     * @param Request $request
     * @return array
     */
    public function getOptionsOfReservationTimes(Request $request)
    {
        $reservationTimesToReturn = [];
        if (is_numeric($request->supplier_id) && !empty($request->date)) {
            $reservationTimes = ReservationTime::getReservationTimesOfTheDay($request->supplier_id, $request->date);
            foreach ($reservationTimes as $reservationTime) {
                if ($reservationTime->reserved>0 || $reservationTime->active==0) {
                    $disabled = true;
                } else {
                    $disabled = false;
                }
                $reservationTimesToReturn[] = [
                    'id'=>$reservationTime->id,
                    'text'=>$reservationTime->reservation_time_from.' - '.$reservationTime->reservation_time_to.' '._e('Kaina', 'admin.reservations').': '.$reservationTime->actualPrice,
                    'disabled'=>$disabled
                ];
            }
        }

        // filtering by search text:
        if (!empty($request->search_text)) {
            $reservationTimesToReturn = array_where($reservationTimesToReturn, function ($time, $key) use ($request) {
                return strpos($time['text'], $request->search_text) !== false;
            });
            $reservationTimesToReturn = array_values($reservationTimesToReturn);
        }

        return [
            'reservation_times'=>$reservationTimesToReturn
        ];
    }

    /**
     * Get sum of order
     * @param Request $request
     * @return array
     */
    public function getSumOfOrder(Request $request)
    {
        $sumOfOrder = OrderItem::getSumOfOrderItems($request->ids_of_existing_order_lines);
        $sumOfOrder = $sumOfOrder + ReservationTime::getSumOfReservationTimes($request->ids_of_new_reservation_times);
        return [
            'sum'=>number_format($sumOfOrder, 2)
        ];
    }

    /**
     * Get info of disabled dates
     * @param Request $request
     * @return array
     */
    public function getDisabledDates(Request $request)
    {
        $dateFrom = $request->date;
        $dateTo = date('Y-m-d', (strtotime($dateFrom) + 60*60*24*365));

        $availableReservationTimes = ReservationTime::getAvailableReservationTimesOfThePeriod($request->supplier_id, $dateFrom, $dateTo);

        $max = end($availableReservationTimes);
        unset($max[3]); //unseting member 'inverted'
        reset($availableReservationTimes);

        return [
            'max'=>$max,
            'disabledDates'=>$availableReservationTimes
        ];
    }
}
